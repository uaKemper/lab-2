object Form1: TForm1
  Left = 266
  Top = 142
  Width = 690
  Height = 418
  Caption = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1085#1072' '#1088#1086#1073#1086#1090#1072' '#8470'2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 223
    Top = 33
    Width = 3
    Height = 326
    Cursor = crHSplit
  end
  object Splitter2: TSplitter
    Left = 449
    Top = 33
    Width = 3
    Height = 326
    Cursor = crHSplit
  end
  object Panel1: TPanel
    Left = 0
    Top = 33
    Width = 223
    Height = 326
    Align = alLeft
    TabOrder = 0
    OnContextPopup = Panel1ContextPopup
    OnResize = Panel1Resize
    object ListBox1: TListBox
      Left = 8
      Top = 30
      Width = 208
      Height = 210
      DragMode = dmAutomatic
      ItemHeight = 13
      Items.Strings = (
        #1055#1110#1076#1089#1080#1083#1077#1085#1085#1103
        #1044#1077#1094#1080#1073#1077#1083
        #1058#1080#1087' '#1087#1110#1076#1082#1083#1102#1095#1077#1085#1085#1103' '#1079#1074#1091#1082#1086#1074#1086#1111' '#1087#1083#1072#1090#1080
        #1042#1080#1088#1086#1073#1085#1080#1082' '#1079#1074#1091#1082#1086#1074#1086#1111' '#1087#1083#1072#1090#1080
        #1044#1080#1072#1087#1072#1079#1086#1085)
      MultiSelect = True
      TabOrder = 0
      OnDragDrop = OnDragDrop
      OnDragOver = OnDragOver
    end
  end
  object Panel2: TPanel
    Left = 226
    Top = 33
    Width = 223
    Height = 326
    Align = alLeft
    TabOrder = 1
    OnResize = Panel2Resize
    object ListBox2: TListBox
      Left = 8
      Top = 30
      Width = 208
      Height = 210
      DragMode = dmAutomatic
      ItemHeight = 13
      Items.Strings = (
        #1058#1080#1087' '#1087#1110#1076#1082#1083#1102#1095#1077#1085#1085#1103' '#1076#1080#1089#1082#1091
        #1052#1072#1082#1089'. '#1086#1073#39#1108#1084' '#1110#1085#1092#1086#1088#1084#1072#1094#1110#1111
        #1056#1086#1079#1084#1110#1088' '#1082#1077#1096#1091
        #1042#1080#1088#1086#1073#1085#1080#1082' '#1076#1080#1089#1082#1091
        #1044#1110#1072#1084#1077#1090#1088' '#1076#1080#1089#1082#1091)
      MultiSelect = True
      TabOrder = 0
      OnDragDrop = OnDragDrop
      OnDragOver = OnDragOver
    end
  end
  object Panel3: TPanel
    Left = 452
    Top = 33
    Width = 222
    Height = 326
    Align = alClient
    TabOrder = 2
    OnResize = Panel3Resize
    object ListBox3: TListBox
      Left = 8
      Top = 30
      Width = 207
      Height = 210
      DragMode = dmAutomatic
      ItemHeight = 13
      Items.Strings = (
        #1052#1072#1082#1089'. '#1087#1088#1086#1087#1091#1089#1082#1085#1072' '#1079#1076#1072#1090#1085#1110#1089#1090#1100
        #1042#1080#1088#1086#1073#1085#1080#1082' '#1084#1077#1088#1077#1078#1085#1086#1111' '#1087#1083#1072#1090#1080
        #1062#1110#1085#1072' '#1084#1077#1088#1077#1078#1085#1086#1111' '#1087#1083#1072#1090#1080
        #1058#1080#1087' '#1087#1110#1076#1082#1083#1102#1095#1077#1085#1085#1103' '#1084#1077#1088#1077#1078#1085#1086#1111' '#1087#1083#1072#1090#1080
        #1044#1088#1072#1081#1074#1077#1088' '#1084#1077#1088#1077#1078#1085#1086#1111' '#1087#1083#1072#1090#1080)
      MultiSelect = True
      TabOrder = 0
      OnDragDrop = OnDragDrop
      OnDragOver = OnDragOver
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 674
    Height = 33
    Caption = 'ToolBar1'
    TabOrder = 3
    object SpeedButton1: TSpeedButton
      Left = 0
      Top = 2
      Width = 57
      Height = 22
      Caption = #1064#1088#1080#1092#1090'+'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 57
      Top = 2
      Width = 57
      Height = 22
      Caption = #1064#1088#1080#1092#1090'-'
      OnClick = SpeedButton2Click
    end
    object ColorBox1: TColorBox
      Left = 114
      Top = 2
      Width = 95
      Height = 22
      ItemHeight = 16
      TabOrder = 0
      OnChange = ColorBox1Change
    end
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 48
    object Item1: TMenuItem
      Caption = #1057#1087#1080#1089#1082#1080
      object N2: TMenuItem
        Caption = #1047#1074#1091#1082#1086#1074#1110' '#1087#1083#1072#1090#1080
        object N11: TMenuItem
          Caption = #1042#1110#1076#1082#1088#1080#1090#1080
          RadioItem = True
          OnClick = N11Click
        end
        object N12: TMenuItem
          Caption = #1047#1072#1082#1088#1080#1090#1080
          OnClick = N12Click
        end
      end
      object N3: TMenuItem
        Caption = #1046#1086#1088#1089#1090#1082#1110' '#1076#1080#1089#1082#1080
        object N13: TMenuItem
          Caption = #1042#1110#1076#1082#1088#1080#1090#1080
          RadioItem = True
          OnClick = N13Click
        end
        object N14: TMenuItem
          Caption = #1047#1072#1082#1088#1080#1090#1080
          OnClick = N14Click
        end
      end
      object N10: TMenuItem
        Caption = #1052#1077#1088#1077#1078#1085#1110' '#1087#1083#1072#1090#1080
        object N15: TMenuItem
          Caption = #1042#1110#1076#1082#1088#1080#1090#1080
          RadioItem = True
          OnClick = N15Click
        end
        object N16: TMenuItem
          Caption = #1047#1072#1082#1088#1080#1090#1080
          OnClick = N16Click
        end
      end
    end
    object Item2: TMenuItem
      Caption = #1042#1080#1075#1083#1103#1076
      object N4: TMenuItem
        Caption = #1047#1073#1110#1083#1100#1096#1080#1090#1080' '#1096#1088#1080#1092#1090
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = #1047#1084#1077#1085#1096#1080#1090#1080' '#1096#1088#1080#1092#1090
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1047#1084#1110#1085#1080#1090#1080' '#1082#1086#1083#1110#1088
        object N17: TMenuItem
          Caption = #1063#1086#1088#1085#1080#1081
          OnClick = N17Click
        end
        object N18: TMenuItem
          Caption = #1063#1077#1088#1074#1086#1085#1080#1081
          OnClick = N18Click
        end
        object N19: TMenuItem
          Caption = #1047#1077#1083#1077#1085#1080#1081
          OnClick = N19Click
        end
      end
    end
    object N1: TMenuItem
      Caption = #1042#1080#1093#1110#1076
      OnClick = N1Click
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 136
    Top = 504
    object N7: TMenuItem
      Caption = #1047#1073#1110#1083#1100#1096#1080#1090#1080' '#1096#1088#1080#1092#1090
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = #1047#1084#1077#1085#1096#1080#1090#1080' '#1096#1088#1080#1092#1090
      OnClick = N8Click
    end
    object N9: TMenuItem
      Caption = #1047#1084#1110#1085#1080#1090#1080' '#1082#1086#1083#1110#1088
      object N20: TMenuItem
        Caption = #1063#1086#1088#1085#1080#1081
        OnClick = N20Click
      end
      object N21: TMenuItem
        Caption = #1063#1077#1088#1074#1086#1085#1080#1081
        OnClick = N21Click
      end
      object N22: TMenuItem
        Caption = #1047#1077#1083#1077#1085#1080#1081
        OnClick = N22Click
      end
    end
  end
end
