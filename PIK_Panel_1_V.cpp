//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PIK_Panel_1_V.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::OnDragDrop(TObject *Sender, TObject *Source,
      int X, int Y)
{
        // ������� ��������� ��� ��������
        TListBox *senderList = (TListBox *)Sender;
        TListBox *sourceList = (TListBox *)Source;

        // �������� �� �������� ������, � ����� ���� ������ ���������
        for (int idx = 0; idx < sourceList->Count; idx++)
        {
                // �� ������� �� �������� ��� �������
                if (sourceList->Selected[idx])
                {
                        //����� �������� ������� � ����� ������ ������
                        senderList->Items->Add(sourceList->Items->Strings[idx]);
                        // ������� ������� � ���� ��������� ������
                        sourceList->Items->Delete(idx);
                        /*
                          ������� �������� ����� index, ��� �� ����������
                          ��������� �������, �� �� ���� �� ��� �����
                          �������� �� � ����� ��������� (�� � ������� ��������,
                          ���� ������ �������� �� 1 � ����� �������)
                        */
                        idx--;
                }
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::OnDragOver(TObject *Sender, TObject *Source,
      int X, int Y, TDragState State, bool &Accept)
{
        // ������������� ����� ��� ���������� �������� ��� ���� ������� �9
        if (    (Source == ListBox1 && Sender == ListBox3) ||
                (Source == ListBox2 && (Sender == ListBox1 || Sender == ListBox3)) ||
                (Source == ListBox3 && Sender == ListBox2)
           )
        {
                // true - ���������� ���������
                Accept = true;
                return;
        }
        // false - ���������� ����������
        Accept = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N1Click(TObject *Sender)
{
        // ��� ���������� ������ "�����" �� ���������� ����� � ��������)
        Close();
}
//---------------------------------------------------------------------------
// ��� �������� ������� ������� ����� ��� ��������� �� ��������� ������
void TForm1::IncreaseFontSize()
{
        ListBox1->Font->Size += 2;
        ListBox2->Font->Size += 2;
        ListBox3->Font->Size += 2;
}
void TForm1::DecreaseFontSize()
{
        ListBox1->Font->Size -= 2;
        ListBox2->Font->Size -= 2;
        ListBox3->Font->Size -= 2;
}
// ������� ������� � ��������� ���� 䳿
void __fastcall TForm1::N4Click(TObject *Sender)
{
        IncreaseFontSize();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N5Click(TObject *Sender)
{
        DecreaseFontSize();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton1Click(TObject *Sender)
{
        IncreaseFontSize();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SpeedButton2Click(TObject *Sender)
{
        DecreaseFontSize();        
}
//---------------------------------------------------------------------------


void __fastcall TForm1::N11Click(TObject *Sender)
{
        ListBox1->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N12Click(TObject *Sender)
{
        ListBox1->Hide();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N13Click(TObject *Sender)
{
        ListBox2->Show();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N14Click(TObject *Sender)
{
        ListBox2->Hide();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N15Click(TObject *Sender)
{
        ListBox3->Show();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N16Click(TObject *Sender)
{
        ListBox3->Hide();        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ColorBox1Change(TObject *Sender)
{
        // ���� ������� ������ ����� � �������
        ListBox1->Font->Color = ColorBox1->Selected;
        ListBox2->Font->Color = ColorBox1->Selected;
        ListBox3->Font->Color = ColorBox1->Selected;
}
//---------------------------------------------------------------------------

// �� ��� �����, ��� ��� ���� ������ ������� ������ ����� ��������������� �� �������
void __fastcall TForm1::Panel1Resize(TObject *Sender)
{
        ListBox1->Width = Panel1->Width - 15;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Panel2Resize(TObject *Sender)
{
        ListBox2->Width = Panel2->Width - 15;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Panel3Resize(TObject *Sender)
{
        ListBox3->Width = Panel3->Width - 15;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Panel1ContextPopup(TObject *Sender,
      TPoint &MousePos, bool &Handled)
{
        PopupMenu1->Popup(Form1->Left + MousePos.x, Form1->Top + MousePos.y);        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N7Click(TObject *Sender)
{
        ListBox1->Font->Size += 2;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N8Click(TObject *Sender)
{
        ListBox1->Font->Size -= 2;    
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TForm1::N17Click(TObject *Sender)
{
        ListBox1->Font->Color = clBlack;
        ListBox2->Font->Color = clBlack;
        ListBox3->Font->Color = clBlack;
        ColorBox1->Selected = clBlack;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N18Click(TObject *Sender)
{
        ListBox1->Font->Color = clRed;
        ListBox2->Font->Color = clRed;
        ListBox3->Font->Color = clRed;
        ColorBox1->Selected = clRed;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N19Click(TObject *Sender)
{
        ListBox1->Font->Color = clGreen;
        ListBox2->Font->Color = clGreen;
        ListBox3->Font->Color = clGreen;
        ColorBox1->Selected = clGreen;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N20Click(TObject *Sender)
{
        ListBox1->Font->Color = clBlack;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N21Click(TObject *Sender)
{
        ListBox1->Font->Color = clRed;        
}
//---------------------------------------------------------------------------

void __fastcall TForm1::N22Click(TObject *Sender)
{
        ListBox1->Font->Color = clGreen;        
}
//---------------------------------------------------------------------------

