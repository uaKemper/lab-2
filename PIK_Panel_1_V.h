//---------------------------------------------------------------------------

#ifndef PIK_Panel_1_VH
#define PIK_Panel_1_VH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TPanel *Panel1;
        TSplitter *Splitter1;
        TPanel *Panel2;
        TSplitter *Splitter2;
        TPanel *Panel3;
        TListBox *ListBox1;
        TListBox *ListBox2;
        TListBox *ListBox3;
        TMenuItem *Item1;
        TMenuItem *Item2;
        TMenuItem *N1;
        TToolBar *ToolBar1;
        TSpeedButton *SpeedButton1;
        TMenuItem *N2;
        TMenuItem *N3;
        TMenuItem *N4;
        TMenuItem *N5;
        TMenuItem *N6;
        TPopupMenu *PopupMenu1;
        TSpeedButton *SpeedButton2;
        TMenuItem *N7;
        TMenuItem *N8;
        TMenuItem *N9;
        TMenuItem *N10;
        TMenuItem *N11;
        TMenuItem *N12;
        TMenuItem *N13;
        TMenuItem *N14;
        TMenuItem *N15;
        TMenuItem *N16;
        TColorBox *ColorBox1;
        TMenuItem *N17;
        TMenuItem *N18;
        TMenuItem *N19;
        TMenuItem *N20;
        TMenuItem *N21;
        TMenuItem *N22;
        void __fastcall OnDragDrop(TObject *Sender, TObject *Source,
          int X, int Y);
        void __fastcall OnDragOver(TObject *Sender, TObject *Source,
          int X, int Y, TDragState State, bool &Accept);
        void __fastcall N1Click(TObject *Sender);
        void __fastcall N4Click(TObject *Sender);
        void __fastcall N5Click(TObject *Sender);
        void __fastcall SpeedButton1Click(TObject *Sender);
        void __fastcall SpeedButton2Click(TObject *Sender);
        void __fastcall N11Click(TObject *Sender);
        void __fastcall N12Click(TObject *Sender);
        void __fastcall N13Click(TObject *Sender);
        void __fastcall N14Click(TObject *Sender);
        void __fastcall N15Click(TObject *Sender);
        void __fastcall N16Click(TObject *Sender);
        void __fastcall ColorBox1Change(TObject *Sender);
        void __fastcall Panel1Resize(TObject *Sender);
        void __fastcall Panel2Resize(TObject *Sender);
        void __fastcall Panel3Resize(TObject *Sender);
        void __fastcall Panel1ContextPopup(TObject *Sender,
          TPoint &MousePos, bool &Handled);
        void __fastcall N7Click(TObject *Sender);
        void __fastcall N8Click(TObject *Sender);
        void __fastcall N17Click(TObject *Sender);
        void __fastcall N18Click(TObject *Sender);
        void __fastcall N19Click(TObject *Sender);
        void __fastcall N20Click(TObject *Sender);
        void __fastcall N21Click(TObject *Sender);
        void __fastcall N22Click(TObject *Sender);
private:	// User declarations
        void IncreaseFontSize();
        void DecreaseFontSize();
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
